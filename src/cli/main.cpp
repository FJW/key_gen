#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdexcept>

#include <boost/program_options.hpp>

#include "key_gen/dictionaries.hpp"
#include "key_gen/picker.hpp"

namespace bpo = boost::program_options;

int main(int argc, char** argv) try {
	bpo::options_description desc("Allowed options");
	desc.add_options()
		("help,h", "Print this help")
		("file,f", bpo::value<std::vector<std::string>>(), "Use file as dictionary")
		("safety,s", bpo::value<unsigned>()->default_value(10u), "the Number of used words per phrase")
		("phrases,n", bpo::value<unsigned>()->default_value(1u), "the Number of generated passphrases");

	bpo::variables_map vm;
	bpo::store(bpo::parse_command_line(argc, argv, desc), vm);
	bpo::notify(vm);
	if (vm.count("help")) {
		std::cout << desc << '\n';
		return 0;
	}

	const auto files = vm.count("file") ? vm["file"].as<std::vector<std::string>>() : std::vector<std::string>{};
	const auto words = vm["safety"].as<unsigned>();
	const auto phrases = vm["phrases"].as<unsigned>();

	if (!vm.count("file")) {
		std::cerr << "Error: no dictinary provided (See --help)\n";
		return 1;
	}

	const auto dict = read_dictionarys(files);
	for(auto i = 0u; i < phrases; ++i) {
		auto phrase = pick(dict, words);
		std::cout << phrase << '\n';
	}
} catch (std::exception& e) {
	std::cerr << "Error: " << e.what() << '\n';
	return 2;
}
