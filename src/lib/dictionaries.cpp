
#include "key_gen/dictionaries.hpp"

#include <algorithm>
#include <fstream>
#include <iterator>
#include <unordered_set>

namespace {

class line: public std::string {
	using std::string::string;
};

std::istream& operator>>(std::istream& stream, line& l) {
	return std::getline(stream, l);
}

}

std::vector<std::string> read_dictionarys(const std::vector<std::string>& files) {
	auto dict = std::unordered_set<std::string>();
	for(auto& filename: files) {
		std::ifstream file{filename};
		std::copy(std::istream_iterator<line>{file}, {}, std::inserter(dict, dict.end()));
	}
	return std::vector<std::string>(dict.begin(), dict.end());
}

