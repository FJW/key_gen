
#include "key_gen/picker.hpp"

#include <stdexcept>
#include <random>

namespace {

std::random_device& rng() {
	thread_local std::random_device rd;
	return rd;
}

std::size_t rand_int(std::size_t max) {
	if (!max) {
		throw std::runtime_error{"empty range"};
	}
	auto dist = std::uniform_int_distribution<std::size_t>{{0}, max - 1u};
	return dist(rng());
}

}

std::string pick(const std::vector<std::string>& dict, unsigned n) {
	const auto wordcount = dict.size();
	auto phrase = std::string{};
	for (auto i = 0u; i < n; ++i) {
		if (i > 0u) {
			phrase += ' ';
		}
		phrase += dict[rand_int(wordcount)];
	}
	return phrase;
}
