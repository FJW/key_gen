
#include  "gui.hpp"

#include <cmath>
#include <iostream>
#include <utility>

#include "key_gen/dictionaries.hpp"
#include "key_gen/picker.hpp"

main_window::main_window(std::vector<std::string> files, unsigned wordcount):
	m_files{std::move(files)},
	m_wordcount_label{"Words per password:"},
	m_wordcount_button{1},
	m_add_dict_button{"add dictionary"},
	m_clear_dicts_button{"clear dictionarys"},
	m_file_list{1},
	m_generate_button{"generate"}
{
	set_title("Key Gen");

	m_add_dict_button.signal_clicked().connect(sigc::mem_fun(*this, &main_window::add_dict));
	m_ctrl_box.add(m_add_dict_button);
	m_clear_dicts_button.signal_clicked().connect(sigc::mem_fun(*this, &main_window::clear_dicts));
	m_ctrl_box.add(m_clear_dicts_button);
	m_ctrl_box.add(m_wordcount_label);
	m_wordcount_button.set_range(1, 100);
	m_wordcount_button.set_value(wordcount);
	m_wordcount_button.set_increments(1,1);
	m_wordcount_button.signal_value_changed().connect(sigc::mem_fun(*this, &main_window::update_entropy_label));
	m_ctrl_box.add(m_wordcount_button);
	m_main_box.pack_start(m_ctrl_box, Gtk::PACK_SHRINK );

	m_file_list.set_column_title(0, "ditionarys");
	set_file_list();
	m_main_box.pack_start(m_file_list, Gtk::PACK_SHRINK );

	m_main_box.pack_start(m_entropy_label, Gtk::PACK_SHRINK);

	m_generate_button.signal_clicked().connect(sigc::mem_fun(*this, &main_window::generate));
	m_main_box.pack_start(m_generate_button, Gtk::PACK_SHRINK );
	m_output.set_wrap_mode(Gtk::WRAP_WORD);
	m_output.set_editable(false);
	m_main_box.add(m_output);
	add(m_main_box);
	set_default_size(400, 400);

	read_dicts();

	show_all();
	if (m_files.empty()) {
		add_dict();
	}
	if (not m_files.empty()) { // User might have cancelled, so this isn't guaranteed
		generate();
	}
	update_entropy_label();
}

void main_window::add_dict() {
	Gtk::FileChooserDialog dialog{*this, "Select Dictionaries"};
	dialog.set_select_multiple(true);
	dialog.set_action(Gtk::FILE_CHOOSER_ACTION_OPEN);
	dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
	dialog.add_button("_Open", Gtk::RESPONSE_OK);
	auto result = dialog.run();
	if (result == Gtk::RESPONSE_OK) {
		const auto files = dialog.get_filenames();
		std::copy(files.begin(), files.end(), std::back_inserter(m_files));
		std::sort(m_files.begin(), m_files.end());
		m_files.erase(std::unique(m_files.begin(), m_files.end()), m_files.end());
		set_file_list();
		read_dicts();
		update_entropy_label();
	}
}

void main_window::clear_dicts() {
	m_files = {};
	m_words = {};
	m_file_list.clear_items();
	update_entropy_label();
}

void main_window::set_file_list() {
	m_file_list.clear_items();
	for(auto& file: m_files) {
		m_file_list.append(file);
	}
}

void main_window::read_dicts() {
	m_words = ::read_dictionarys(m_files);
}

void main_window::generate() {
	if (m_words.empty()) {
		Gtk::MessageDialog dialog{*this, "No Words in wordlist", false, Gtk::MESSAGE_ERROR};
		dialog.run();
		return;
	}
	const auto wordcount = static_cast<unsigned>(m_wordcount_button.get_value_as_int());
	read_dicts();
	const auto password = pick(m_words, wordcount);
	m_output.get_buffer()->set_text(password);
}

void main_window::update_entropy_label() {
	const auto wordcount = m_words.size();
	if (wordcount == 0) {
		m_entropy_label.set_text("Empty dictionary, no Passwords can be generated");
		return;
	}
	const auto entropy_per_word = std::log2(wordcount);
	const auto words_in_pw = m_wordcount_button.get_value_as_int();
	const auto total_entropy = entropy_per_word * words_in_pw;
	const auto text = 
		std::to_string(wordcount) + " words in wordlist, "
		+ std::to_string(entropy_per_word) + " Bits entropy each * "
		+ std::to_string(words_in_pw) + " = " + std::to_string(total_entropy)
		+ " Bits of entropy per password";
	m_entropy_label.set_text(text);
}

