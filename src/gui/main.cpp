
#include <vector>
#include <string>

#include "gui.hpp"

int main(int argc, char** argv) {
	const auto files = std::vector<std::string>(argv+1, argv+argc);
	Gtk::Main kit(argc, argv);
	main_window window{files, 10};
	Gtk::Main::run(window);
}
