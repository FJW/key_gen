
#ifndef GUI_HPP
#define GUI_HPP

#include <gtkmm.h>


class main_window: public Gtk::Window {
public:
	main_window(std::vector<std::string> files, unsigned wordcount);
private:
	void read_dicts();
	void clear_dicts();
	void generate();
	void add_dict();
	void set_file_list();
	void update_entropy_label();

	std::vector<std::string> m_files;
	std::vector<std::string> m_words;

	Gtk::VBox m_main_box;
	Gtk::HBox m_ctrl_box;
	Gtk::Label m_wordcount_label;
	Gtk::SpinButton m_wordcount_button;
	Gtk::Button m_add_dict_button;
	Gtk::Button m_clear_dicts_button;
	Gtk::ListViewText m_file_list;
	Gtk::Label m_entropy_label;
	Gtk::Button m_generate_button;
	Gtk::TextView m_output;
};


#endif
