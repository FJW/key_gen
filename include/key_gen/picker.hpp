#ifndef PICKER_HPP
#define PICKER_HPP

#include <string>
#include <vector>

std::string pick(const std::vector<std::string>& dict, unsigned n);


#endif
